# Set backup directory
BACKUP_DIR=/path/to/bup/dir

msg_log() {
  # Send log to syslog
  logger -t `basename $0` "$*"

  # Send echo to stdout
  LOG_TS=`date +'%H:%M:%S'`
  echo "$LOG_TS - $*"
}

DAY_NAME=`date '+%a'`

#Setting filename and extension

# Comment out uncompressed sql file if you are using ard
# BACKUP_FILE=$BACKUP_DIR/SITENAME.ENV.$DAY_NAME.sql

# Comment out tgz if you are using sql-dump
BACKUP_FILE=$BACKUP_DIR/SITENAME.ENV.$DAY_NAME.tgz

msg_log "Backing up database to $BACKUP_FILE..."

# Comment out drush sql-dump, to use ard instead
# drush @SITEALIAS sql-dump --gzip --result-file=$BACKUP_FILE -y

# Comment out drush ard, to use sql-dump instead ^
drush @SITEALIAS archive-dump \
 --destination=$BACKUP_FILE \
 --preserve-symlinks \
 --overwrite


RC=$?

if [ "$RC" = 0 ]; then
  msg_log "Backup was completed successfully :)"
else
  msg_log "Backup failed :( exited with return code: $RC"
fi
