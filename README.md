# Useful scripts
---


# drush\_daily_bup.sh


I use this simple bash script along with cron to run a daily backup for Drupal sites using drush ([drush ard](https://drushcommands.com/drush-8x/core/archive-dump/) or [drush sql-dump](https://drushcommands.com/drush-8x/sql/sql-dump/) ).


The 'default' will use `drush ard`, but I added a commented out line for `drush sql-dump` just in case.


### The variables that should be edited are:
* **BACKUP_DIR** --> The path to the backup directory
* **SITE_NAME** --> The site tag, used for the backup filename. I usually include the env here (dev, stage, prod, etc...) Like: "sitename.prod"
* **DRUSH_ALIAS** --> The Drush alias set for the site (usually in sitename.aliases.drushrc.php or aliases.drushrc.php )

### Then, remember to make the script executable running:
`chmod 755 /path/to/the/script/drush_daily_backup.sh`

### Using cron to schedule the backup. For example, run script daily at 3:33 am.
`33 3 * * * /path/to/the/script/drush_daily_backup.sh`

**NOTE:** Usually is a good idea to check the backup directory permissions, so the backups are not readable for everyone!

**NOTE2:** Its also a good idea to have backups in different locations, specially outside the site server

---
